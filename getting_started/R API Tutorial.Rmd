

This tutorial will show you how to access information from your community's instance of Looker using R.

Python is the recommended tool for querying the API, as looker's R Software Development Kit (SDK) is no longer updated. However, retrieving data for analysis is still possible in the R Looker SDK.

# Installing R and Rstudio

First, make sure you have the most recent versions of R and Rstudio installed. 

R can be installed from the Comprehensive R Archive Network (CRAN) here: https://cran.r-project.org/

![Click "Download R for Windows" or "Download R for macOS" to download R](images/CRAN_Download_Screen.png)


Rstudio is an Integrated Development Environment for R, which can be downloaded and installed here: https://www.rstudio.com/products/rstudio/download/


Once you have R and Rstudio installed, you'll need to install some extensions to R called packages so that you can use the looker API to analyze the data from your community's Clarity HMIS in R.


# Intro to R and Rstudio

Rstudio is the most popular integrated development environment for R, and is used very often to work with data analysis, data science, graphics, and package development. 

The RStudio interface contains 4 panes.

The pane on the bottom left is the console. This is what you would see if you ran R on its own. You might be familiar with the console if you've used other programming languages. You can type in a command, press return, and the computer executes the command.

![Rstudio Console](images/Rstudio Console pane.png)

 The pane on the top left is the script editor. This allows you to write scripts so that you don't have to remember each command you wrote. This is where you'll write the majority of your code. 

![Rstudio Script Editor](images/Rstudio Script Editor.png)


The pane on the top right is the Environment pane. This shows the data that is currently active in the environment. 

![Rstudio Environment Pane](images/Rstudio Environment Pane.png)

The pane on the bottom right contains the file explorer, plots, packages, help, and viewer. This is the pane that will change in function most often, since you'll use each of those tabs often. 
File explorer lets you see what files are on your computer.
Plots displays whatever plots you've run. 
Packages shows what packages you have installed, and the functions included in those packages. 
Help will be very useful to you early on. All R functions have a help file that explains the function and what to feed into it to get the desired result. 
Viewer displays interactive content like plotly charts or shiny apps. 

![Rstudio Files Pane](images/Rstudio Files Pane.png)

If you're interested in learning more about doing data science with R and Rstudio, I suggest checking out the following resources: 

Rstudio Cheatsheets: <https://www.rstudio.com/resources/cheatsheets/>

Free E-Book R For Data Science, by Hadley Wickham (Creator of Rstudio): <https://r4ds.had.co.nz/>

Quick-R, an easy to use resource for statistics in R: <https://www.statmethods.net/>

Datacamp's Free Introduction to R course: <https://www.datacamp.com/courses/free-introduction-to-r>

# Install the lookr package

You'll use the lookr package to connect to looker.  

The lookr package was created by the looker developers, but it is no longer updated. However, the important functions we will go over in this tutorial still work well.

The lookr package is also not an official R package, so we have to install it directly from its repository on Github. This is easy to do, as long as you have the devtools package installed. Normally,however, I would advise you not to install packages from github unless you trust the source of the package.

You can install packages using the `install.packages()` command in your script or in the console. We'll need to install the devtools package because lookr is not in the official CRAN repository. Devtools allows you to install packages from github or other sources, though it's usually wise to stick to the packages on CRAN. 
```{r}
#Install the lookr package from github. This package was created by looker devs, but is no longer supported.
#install.packages("devtools")
#devtools::install_github("looker/lookr")

library(lookr)
```

You can also use the Rstudio interface to install a package. 

In the packages tab of the bottom-right pane, there is a button labeled "Install". A pop-up menu should appear that allows you to type the name of the package you would like to install. The interface will then run the code that installs the packages for you. 

![This is the install packages button in the bottom right pane](images/install packages button.png)

![This is the install packages window. Type the name of your package here and click install.](images/install packages interface.png)



# Set up Looker API Credentials

Once you've gotten your API credentials by submitting a ticket to the Clarity Helpdesk, you will need to create a .ini file containing your credentials for the Looker API. It is extremely important that you do not store these credentials as text in one of your scripts, because your API credentials are essentially the same as your username and password. 

<b> You need to be extremely careful where you store your API credentials </b>

This is the format required for your .ini file: 

```{}
# API version is required
api_version=4.0
# Base URL for API. Do not include /api/* in the url
base_url=https://looker.clarityhs.com:19999
# API 3 client id, obtained from looker
client_id=your_API3_client_id
# API 3 client secret, obtained from looker
client_secret=your_API3_client_secret
# Set to false if testing locally against self-signed certs. Otherwise leave True
verify_ssl=True
```

Make sure you save your .ini file in a secure location on your machine that only you can access. The information in the .ini file contains your permissions to the Looker system. 


# Connecting to the Looker API in R

```{r }
sdk <- LookerSDK$new(configFile="my_config_file.ini")
```

Note: When using R, you'll have to set your working directory to the location where the config ini file is located or call the entire file path for the ini file. 
To set your working directory, use the Set working directory option in the Session menu at the top of the screen. 

This will open up the connections pane in Rstudio. You should see your community's looker instance as "clarity_base_mycommunity"

![You'll see the connection to your community here](images/Rstudio Connections Pane.png)

Once you have put in your credentials, you can use the Looker API to obtain the results of a saved look, and create your own query. 

# Retrieve results of a look

Looks are saved queries or visualizations. Each look has its own identifier, which you can find in the URL of the look. (e.g. Look 79222 has a url of looker.clarityhs.com/looks/79222)

Find or make a look you'd like to look at with R and make a note of the look number. 

Be careful to use the correct capitalization in your R commands!

```{r}
lookdata <- sdk$runLook(lookId = 79222)
```

This look is a visualization of a table. 

![This is what the look we downloaded looks like in Looker](images/Look_Example.png)

The runLook command returns a list of lists where each row of the table in the look is a list of each entry in the row. 

![The runLook commmand returns a list of lists where each row is a list of each entry in the row](images/list of lists in script viewer.png)

This is different from the format most R users prefer to work in, so let's convert it to a data frame using the `sapply` and `data.frame` commands. 

```{r}
#Get column names
looknames <- names(lookdata[[1]])

#Use sapply to make each column its own vector. 

agencies.name = sapply(lookdata, "[[",1)
agencies.id = sapply(lookdata,"[[",2)
agencies.coc = sapply(lookdata,"[[",3)
data_quality.dob_error_count = sapply(lookdata,"[[",4)
data_quality.name_error_count = sapply(lookdata,"[[",5)
dq_client_programs.end_date_error_count = sapply(lookdata,"[[",6)
dq_client_programs.start_date_error_count = sapply(lookdata,"[[",7)

#Join the vectors together to make a data frame
df <- data.frame(agencies.name, agencies.id, agencies.coc, data_quality.dob_error_count,
                 data_quality.name_error_count, dq_client_programs.end_date_error_count,
                 dq_client_programs.start_date_error_count)

#You could also use an lapply loop to convert the looker API output to a data frame. This makes the code more concise. 

df_lookdata <- as.data.frame(
                  lapply(1:length(names(lookdata[[1]])), 
                         function(x) sapply(lookdata,"[[",x))
                            )

names(df_lookdata) <-  names(lookdata[[1]])

```


# Run a Looker Inline Query

You can also directly retrieve the results of a query you specify from an API command. 

You'll need to know the name of your instance, the name of the model you're referencing (e.g., Data Quality, HMIS Performance, Client, Coordinated Entry), and the fields you want to extract. 

```{r}

data <- sdk$runInlineQuery(model="base_demo_model", #This is the name of your looker/Clarity instance
                           view="data_quality", # This is the name of the model you are querying
                           fields=c("agencies.name",
                                    "agencies.id",
                                    "agencies.coc",
                                    "data_quality.dob_error_count",
                                    "data_quality.name_error_count",
                                    "dq_client_programs.end_date_error_count",
                                    "dq_client_programs.start_date_error_count")
                                    )

```


You can also add filters, sorts, limits, and the query Timezone into the runInlineQuery command.
```{r}

custom_query_data <- sdk$runInlineQuery(model="base_demo_model", #This is the name of your looker/Clarity instance
                           view="data_quality", # This is the name of the model you are querying
                           filters = list(agencies.coc= jsonlite::unbox("CA-500") ),
                           fields=c("agencies.name",
                                    "agencies.id",
                                    "agencies.coc",
                                    "data_quality.dob_error_count",
                                    "data_quality.name_error_count",
                                    "dq_client_programs.end_date_error_count",
                                    "dq_client_programs.start_date_error_count"),
                           limit=500,
                           sorts = c("agencies.name"),
                           queryTimezone = "America/Los_Angeles"
                                    )
#Transform into an analyzable data frame
dfnames <- names(custom_query_data[[1]])

df_cqd <- as.data.frame(
                  lapply(1:length(dfnames), 
                        function(x) sapply(custom_query_data,"[[",x))
                        )
names(df_cqd) <- dfnames
```

The fields section of the code corresponds to the fields that you select in looker.
The filters section of the code corresponds to any filters you create in the query.
![The fields you choose in Looker correspond to the R code for fields. The filters you choose in Looker correspond to the R code for filters](images/explore v code fields filters.png)

The limit parameter corresponds to the "Row Limit" that you can set in Looker.
The sorts parameter corresponds to clicking a field to sort by that field in Looker.
![The limits and sorts also correspond to code in the R query](images/explore v code sorts limits.png)

Now that you know how to combine the power of looker and R, you can perform any analysis you can think up! 
